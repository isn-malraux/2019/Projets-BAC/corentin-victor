# Corentin-Victor

Projet de BAC Spécialité ISN session 2019 : Pong

## Projet :

Création d'une copie d'un jeu populaire nommé "Pong" jouable à 2 joueurs en local ou en réseau local. C'est une copie partielle sans l'intrégalité du contenu. Une version donc simpliste en prenant compte de nos capacités et notre niveau.
Notre jeu se nommera "Ping".

## Objectifs :

→ Gameplay identique ou presque au jeu de base fonctionnel dans la mesure du possible.
→ Limiter voir supprimer tout bug potentiel dans notre programme.
→ Un affichage clair et ergonomique pour le/les joueurs comportant un menu et une interface.


## Fonctionnalités :

→ Présence d'une interface graphique simpliste mais claire et fonctionnelle.
→ Possibilité de jouer à deux joueurs réels (en réseau local). 
→ Présence d'un compteur de point : score
→ Présence d'un chronomètre : temps